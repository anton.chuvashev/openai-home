import os
from flask import Flask
from flask import request, render_template, make_response, send_from_directory
from ai import request_ai

app = Flask(__name__)


@app.route("/")
def index():
    return make_response(render_template('index.html'))


@app.route("/ai", methods=['POST'])
def ai():
    return request_ai(request.json)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')
