from pprint import pprint

import openai


def request_ai(request_params: dict):
    version = request_params["version"]

    if version == '4':
        return request_v4(request_params)
    elif version == '3':
        return request_v3(request_params)
    else:
        return 'Version required'


def request_v3(request_params: dict):
    text = request_params["text"]
    temperature = float(request_params["temp"] or 1)
    max_tokens = int(request_params["size"] or 500)

    if not text: return 'Empty request text'

    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {
                "role": "user",
                "content": request_params["text"]
            }
        ],
        temperature=temperature,
        max_tokens=max_tokens,
        frequency_penalty=0.0
    )
    return response.choices[0].message.content


def request_v4(request_params: dict):
    text = request_params["text"]
    temperature = float(request_params["temp"] or 0.5)
    max_tokens = int(request_params["size"] or 500)

    if not text: return 'Empty request text'

    try:
        response = openai.ChatCompletion.create(
            model="gpt-4",
            messages=[
                {
                    "role": "user",
                    "content": request_params["text"]
                }
            ],
            temperature=temperature,
            max_tokens=max_tokens,
            frequency_penalty=0.0
        )

        return response.choices[0].message.content

    except openai.error.OpenAIError as e:
        return f"OpenAI API error: {e}"

    except Exception as e:
        return f"Error: {e}"
