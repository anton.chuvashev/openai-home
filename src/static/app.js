function App() {
  const [inputText,       setInputText]       = React.useState('');
  const [inputVersion,    setInputVersion]    = React.useState('3');
  const [inputTemp,       setInputTemp]       = React.useState('0.25');
  const [inputSize,       setInputSize]       = React.useState('500');
  const [responseContent, setResponseContent] = React.useState('');
  const [inProgress,      setInProgress]      = React.useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    setInProgress(1);

    const config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
            {
                text:    inputText,
                temp:    inputTemp,
                size:    inputSize,
                version: inputVersion,
            }
        )
    }
    const response = await fetch(`/ai`, config);
    const data = await response.text();

    setInProgress(0);

    setResponseContent(data);
  };

  const handleSelect = (e) => {
    console.log(e);
  }

  const handleTaKeyDown = (e) => {
    if (e.ctrlKey && e.key === 'Enter') {
      e.preventDefault();
      handleSubmit(e);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
        <div className="container">
            <div className="row">
                <div className="col-12 text-center">
                    <h5>Future is here &#128578;</h5>
                </div>
            </div>
            <div className="row">
                <div className="col-9 col-sm-10 col-lg-11">
                    <textarea
                        className="border-primary col-11 mb-2"
                        style={{"width":"calc(100% + 15px)","height":"calc(100% - 8px)","marginLeft":"-11px"}}
                        type="text"
                        rows="10"
                        id="input"
                        value={inputText}
                        onChange={(e) => setInputText(e.target.value)}
                        onKeyDown={handleTaKeyDown}
                    />
                </div>
                <div className="col-3 col-sm-2 col-lg-1 mb-2 border border-primary text-center" style={{"paddingTop":"45px"}}>
                    <div className="dropdown">
                      <button
                        className="btn btn-sm btn-outline-warning mb-1 dropdown-toggle"
                        type="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                        style={{"width":"60px"}}
                        onSelect={handleSelect}
                      >
                      {inputVersion}
                      </button>
                      <ul className="dropdown-menu">
                        <li><a onClick={(e) => setInputVersion(e.target.text)} className="dropdown-item" href="#">3</a></li>
                        <li><a onClick={(e) => setInputVersion(e.target.text)} className="dropdown-item" href="#">4</a></li>
                      </ul>
                    </div>
                    <div className="dropdown">
                      <button
                        className="btn btn-sm btn-outline-warning mb-1 dropdown-toggle"
                        type="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                        style={{"width":"60px"}}
                        onSelect={handleSelect}
                      >
                      {inputTemp}
                      </button>
                      <ul className="dropdown-menu">
                        <li><a onClick={(e) => setInputTemp(e.target.text)} className="dropdown-item" href="#">0</a></li>
                        <li><a onClick={(e) => setInputTemp(e.target.text)} className="dropdown-item" href="#">0.2</a></li>
                        <li><a onClick={(e) => setInputTemp(e.target.text)} className="dropdown-item" href="#">0.5</a></li>
                        <li><a onClick={(e) => setInputTemp(e.target.text)} className="dropdown-item" href="#">0.8</a></li>
                        <li><a onClick={(e) => setInputTemp(e.target.text)} className="dropdown-item" href="#">1.0</a></li>
                        <li><a onClick={(e) => setInputTemp(e.target.text)} className="dropdown-item" href="#">1.5</a></li>
                      </ul>
                    </div>
                    <div className="dropdown">
                      <button
                        className="btn btn-sm btn-outline-warning mb-1 dropdown-toggle"
                        type="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                        style={{"width":"60px"}}
                      >
                      {inputSize}
                      </button>
                      <ul className="dropdown-menu">
                        <li><a onClick={(e) => setInputSize(e.target.text)} className="dropdown-item" href="#">100</a></li>
                        <li><a onClick={(e) => setInputSize(e.target.text)} className="dropdown-item" href="#">500</a></li>
                        <li><a onClick={(e) => setInputSize(e.target.text)} className="dropdown-item" href="#">1000</a></li>
                        <li><a onClick={(e) => setInputSize(e.target.text)} className="dropdown-item" href="#">2000</a></li>
                        <li><a onClick={(e) => setInputSize(e.target.text)} className="dropdown-item" href="#">4096</a></li>
                      </ul>
                    </div>
                    <button
                        style={{"width":"60px"}}
                        className="btn col-1 mb-1 btn-outline-danger btn-sm position-relative end-0" type="button"
                        onClick={(e) => setInputText('')}
                    >
                        <p style={{"marginTop":"-1px", "marginBottom":"1px"}} >X</p>
                    </button>
                    <button
                        style={{"width":"60px"}}
                        className="btn col-1 mb-1 btn-outline-primary btn-sm position-relative end-0" type="submit"
                    >
                        <span style={{"marginTop":"2px", "marginBottom":"-2px"}} className={inProgress ? 'spinner-border spinner-border-sm' : 'visually-hidden'} role="status" aria-hidden="true"></span>
                        <span style={{"marginTop":"-1px", "marginBottom":"1px"}} className={inProgress ? 'visually-hidden' : ''}>&#9658;</span>
                    </button>
                </div>
            </div>
            <div className="row">
                <textarea
                    className="col-12 border-primary"
                    rows="20"
                    id="textarea"
                    defaultValue={responseContent}
                />
            </div>
        </div>
    </form>
  );
}

const container = document.getElementById('app');
const root = ReactDOM.createRoot(container);
root.render(<App />);
