FROM python:3.10-alpine
WORKDIR /src

RUN apk --update --upgrade add --no-cache  gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev

RUN python -m pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5001
COPY . /
CMD ["gunicorn", "--reload", "--bind", "0.0.0.0:5001", "app:app"]
